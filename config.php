<?php

return [
    'production' => true,
    'baseUrl' => '/ngala-jigsaw',
    'base' => '/ngala-jigsaw',
    'title' => 'Jigsaw',
    'description' => 'Example Jigsaw site using GitLab Pages',
    'collections' => [],
];
